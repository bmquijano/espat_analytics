$( document ).ready(function() {
    
	$('.alpha').click(ajax);
	
	$('.menu .item')
	  .tab()
	;
	
	$('.beta').click(ajax2);
	
	$('.run-arima').click(ajax3);
	
	$('.sample-format').click(sample_format);
	
	
	
});

function sample_format(){
	
	$.ajax({
 
    url: "step_one_download",
    data: {
        id: 123
    },
    type: "GET",
	})
  .done(function( json ) {
	 alert( "Success!" );
  })
  .fail(function( xhr, status, errorThrown ) {
    alert( "Sorry, there was a problem!" );
  
  });
 
}

function ajax(){
	
	$('.loading-data').css('display','block');
	$.ajax({
 
    url: "step_two_ajax",
    data: {
        id: 123
    },
    type: "GET",
    dataType : "json",
})
  .done(function( json ) {
	  src = $('.graph-step-two').attr('src');
	  $('.img1').attr('src', src + json.image1);
	  $('.img2').attr('src', src + json.image2);
	  $('.img3').attr('src', src + json.image3);
	  $('.container-graph').css('display','block');
	  $('.loading-data').css('display','none');
	  $('.alpha').css('display','none');
	  $('.step-two-next').css('visibility','visible');
  })
  .fail(function( xhr, status, errorThrown ) {
    alert( "Sorry, there was a problem!" );
  
  });
 
}

function ajax2(){
	
	$('.loading-data-arima').css('display','block');
	$.ajax({
 
    url: "step_three_ajax",
    data: {
        id: 123
    },
    type: "GET",
    dataType : "json",
})
  .done(function( json ) {
	  $('.container-arima').css('display','block');
	  $('.loading-data-arima').css('display','none');
	  $('.beta').css('display','none');	  
	  $('.auto-arima').html(json.output);
	  $('#p-val').val(json.d[0]);
	  $('#d-val').val(json.d[1]);
	  $('#q-val').val(json.d[2]);
	  
	 // setTimeout(click_run_arima, 5);
	  
	  
  })
  .fail(function( xhr, status, errorThrown ) {
    alert( "Sorry, there was a problem!" );
  
  });
 
}

function click_run_arima(){
	$('.run-arima').trigger('click');
}

function ajax3(){
	$('.run-arima').toggleClass('loading');
	$('.arima-output').html('');
	$('.forecast-graph').css('display','none');
	pval = $('#p-val').val();
	dval = $('#d-val').val();
	qval = $('#q-val').val();
	ci = $('#ci').val();
	
	$.ajax({
 
    url: "step_three_arima_ajax",
    data: {
        id: 123,
		p : pval,
		d: dval,
		q: qval,
		c: ci 
    },
    type: "GET",
    dataType : "json",
})
  .done(function( json ) {
	  $('.run-arima').toggleClass('loading');
	  t2 = '<table class="ui celled table">'+tabular(json.fut,ci)+'<table>';
	  $('.arima-output').html(t2);
	  $('.forecast-graph').css('display','block');
	  $('.step-three-next').css('visibility','visible');
	  img = STATIC + json.forecast;
	  $('.graph-step-three').attr('src', img);
	  
  })
  .fail(function( xhr, status, errorThrown ) {
    alert( "Sorry, there was a problem!" );
  
  });
 
}


function tabular(t,ci){
	head = '<thead><tr><th>Month</th><th>Forecast</th><th>Lo '+ci+'</th><th>Hi '+ci+'</th></tr></thead>';
	body = '';
	for(i=0; i< t.length; i++){
		
		inner = '';
		for(j = 0; j < t[i].length; j++){
			inner += '<td>'+ t[i][j] +'</td>';
			
		}
		inner2 = '<tr>' + inner + '</tr>'; 
		body += inner2;

	}
	
	body = '<tbody>' + body + '</tbody>';
        
	return head + body;
	
}

