
function save_new_password(){
	
	var p1 = $('#new-password').val();
	var p2 = $('#retype-new-password').val();
	
	if(p1 != '' && p2 != ''){
		if(p1 == p2){
			change_password(p1);
		}else{
			$('#new-password').parent().parent().addClass('error');
			$('#retype-new-password').parent().parent().addClass('error');
		}
	}else{
		$('#new-password').parent().parent().addClass('error');
		$('#retype-new-password').parent().parent().addClass('error');
	}
	
}


function change_password(np){
	$.ajax({
 
    url: "change_password",
    data: {
        new_password: np,
		'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()
    },
    type: "POST",
})
  .done(function( json ) {
	  $('.pop-password')
		.modal('hidden')
		;
	 location.reload();
  })
  .fail(function( xhr, status, errorThrown ) {
    alert( "Sorry, there was a problem!" );
  
  });
 
}