$( document ).ready(function() {
    
	
	$('#create-button').click(check_fields);

		;
	$('.add-user').click(function(){
		$('.add-user-modal').modal('show')
		;
		
	});
	

	
	
	$('.view').click(function(){
		$('.history-modal')
		  .modal('show')
		;
		
		get_info($(this).data('value'));
	});
	

	$('.delete').click(function(){
		$('.delete-modal')
		  .modal('show')
		;
		ID_delete = $(this).data('value')
		$('.delete-account').html($(this).data('name'));
	});
	
	
	$('[id^="suspend"]').click(function(){
		$('.suspend-modal')
		  .modal('show')
		;
		$('.suspend-account').html($(this).data('name'));
		ID_suspend = $(this).data('value');
	});
	
	$('.suspend-confirm').click(function(){
		$('.suspend-modal')
		  .modal('hide')
		;
		suspend(ID_suspend);
		
	});
	
	$('.delete-confirm').click(function(){
		$('.delete-modal')
		  .modal('hide')
		;
		delete_acct(ID_delete);
		
	});
	
	
	
	
});

function check_fields(){
	$('.field').removeClass('error');
	var ctr = 0;
	if($('#username').val() == ''){
		ctr += 1;
		$('#username').parent().addClass('error');
	}
	
	if($('#email').val() == ''){
		ctr += 1;
		$('#email').parent().addClass('error');
	}
		
	if($('#first-name').val() == ''){
		$('#first-name').parent().addClass('error');
		ctr += 1;
	}
	if($('#designation').val() == ''){
		$('#designation').parent().addClass('error');
		ctr += 1;
	} 
	if($('#last-name').val() == ''){
		$('#last-name').parent().addClass('error');
		ctr += 1;
	}
	if($('#password').val() == ''){
		$('#password').parent().addClass('error');
		ctr += 1;
	} 
	if($('#repassword').val() == ''){
		$('#repassword').parent().addClass('error');
		ctr += 1;
	}
	if($('#password').val() != $('#repassword').val()){
		$('#password').parent().addClass('error');
		$('#repassword').parent().addClass('error');
		ctr += 1;
	}else{
		$('#password').parent().removeClass('error');
		$('#repassword').parent().removeClass('error');
	
	}
	
	var dd = $('.dropdown')
	  .dropdown('get value')
	;
	
	if(ctr == 0){
		$('#create-submit').trigger('click');
	}
	

}

function get_info(id){
	$.ajax({

	url: "get_history",
	type: "POST",
	data: {'id': id, 'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()}
	})
  .done(function( json ) {
	str = '<table class="ui celled table"><thead><tr><th>No.</th>';
	str += '<th>Date time of generating report</th></tr></thead><tbody>';
	
	$.each(json['stamps'], function(i, obj) {
		str += '<tr><td data-label="Number">'+(i+1)+'</td>';
		str += '<td data-label="Datetime">'+obj+'</td></tr>';	
	});
	
	 str += '</tbody></table>';
	 $('.scrolling.content').html(str);
	
  })
  .fail(function( xhr, status, errorThrown ) {
	alert( "Sorry, there was a problem!" );
  
  });


}
var ID_suspend = null;
var ID_delete = null;
function suspend(id){
	$.ajax({

	url: "suspend_account",
	type: "POST",
	data: {'id': id, 'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()}
	})
  .done(function( json ) {
	if(json.status){
		$('#user-'+id).html('<i class="green check circle icon"></i>');
		$('#suspend-'+id).removeClass('green');
		$('#suspend-'+id).addClass('yellow');
		$('#suspend-'+id).html('Suspend');
	}else{
		$('#user-'+id).html('<i class="red times circle icon"></i>');
		$('#suspend-'+id).addClass('green');
		$('#suspend-'+id).removeClass('yellow');
		$('#suspend-'+id).html('Activate');
	}
		
  })
  .fail(function( xhr, status, errorThrown ) {
	alert( "Sorry, there was a problem!" );
  
  });


}

function delete_acct(id){
	$.ajax({

	url: "delete_account",
	type: "POST",
	data: {'id': id, 'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()}
	})
  .done(function( json ) {
	location.reload();
		
  })
  .fail(function( xhr, status, errorThrown ) {
	alert( "Sorry, there was a problem!" );
  
  });


}
