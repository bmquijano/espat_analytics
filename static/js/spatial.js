$( document ).ready(function() {
//readDataFile();
	getInfo();

	var slider_sd = document.getElementById("range-sd");
	output_sd = document.getElementById("period");
	output_sd.innerHTML = ""; // Display the default slider value
	
	slider_sd.oninput = function() {
		dengue_change_map(this.value);
	}
});

function readDataFile(){
	MY_URL = HOST + "analytics/data_csv";
	
	// Create XHR and FileReader objects
    var xhr = new XMLHttpRequest(),
        fileReader = new FileReader();

    xhr.open("GET", MY_URL, true);
    // Set the responseType to blob
    xhr.responseType = "blob";

    xhr.addEventListener("load", function () {
        if (xhr.status === 200) {
            // onload needed since Google Chrome doesn't support addEventListener for FileReader
            fileReader.onload = function (evt) {
                // Read out file contents as a Data URL
                var contents = evt.target.result;
				parseDengue(contents);
				
            };
            // Load blob as Data URL
            fileReader.readAsText(xhr.response);
        }
    }, false);
    // Send XHR
    xhr.send();
	
}

function create_table(contents){
	var tab_data = '<table class="ui selectable celled table table-data"><thead><tr><th class="time-header">T&nbsp;i&nbsp;m&nbsp;e</th>';
	
	contents['columns'].forEach(function(entry) {
		var result = entry.replace( /([A-Z])/g, " $1" );
		var finalResult = result.charAt(0).toUpperCase() + result.slice(1);
		tab_data += '<th>'+finalResult+'</th>'
	});

	tab_data += '</tr></thead><tbody>';
	
	for(a=0; a<contents['time'].length;a++){
		tab_data += '<tr><td>'+contents['time'][a]+'</td>';
		contents['columns'].forEach(function(entry) {
			tab_data += '<td>'+contents[entry][a]+'</td>';
		});
		tab_data += '</tr>';
		
	}
	
	tab_data += '</tbody></table>';

	$('#data-table').html(tab_data);
	
}

function get_dengue_data(){
	
	$.ajax({
 
    url: "data_csv",
    type: "GET",
	})
  .done(function( json ) {
	csv = json;
	change_val(1);
	 
	$('.spatial-container').css('visibility','visible');
	$('#range-sd').css('display','block');
	create_table(csv);
  })
  .fail(function( xhr, status, errorThrown ) {
    alert( "Sorry, there was a problem!" );
  
  });
 
}
var csv = null;
function parseDengue(content){
	//alert(content);
}
function dengue_change_map(val){
	$('#period').html(val);
	change_val(val);
}

var COLS = null;
function getInfo(){
	$.ajax({
		url: "get_info",
		type: "GET"
		})
	  .done(function( data ) {
		 COLS = data['cols'];
		 get_dengue_data();
		
	  })
	  .fail(function( xhr, status, errorThrown ) {
		alert( "Sorry, there was a problem!" );
	  
	});
	
}
var dengue_data;

function popData(){
	dengue_data = [];

	for(a=0; a < COLS.length; a++){
		temp = [COLS[a]];
		dengue_data.push(temp);
	}

}


function change_val(i){
	$('#period').html(csv['time'][i-1]);

	popData();
		
	for(a=0; a<csv['columns'].length; a++){
		col = csv['columns'][a];
		
		dengue_data[a].push(csv[col][i-1]);
		
	}
	show_map(dengue_data, csv['time'][i-1],csv['info']);
}
function show_map(dengue_data, time, geojson){
	console.log(dengue_data);
    // Initiate the chart
    Highcharts.mapChart('dengue-map', {
        chart: {
            map: shp[geojson['shp']],
			height: 700
        },

        title: {
            text: geojson['province']
        },
		
		subtitle: {
			text: data_name + ' Occurrence per '+ geojson['div'] +' ('+time+')'
		},

        mapNavigation: {
            enabled: false,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            tickPixelInterval: 100
        },
		
		tooltip: {
			formatter: function () {
				return 'The number of occurrence/s for <b>' + this.point.properties.NAME_2 +
					'</b> is <b>' + this.series.valueData[this.point.x] + '</b>';
			}
		},

        series: [{
            data: dengue_data,
            keys: ['NAME_2', 'value'],
            joinBy: 'NAME_2',
            name: 'Number of occurrences',
            states: {
                hover: {
                    color: '#a4edba'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.properties.NAME_2}'
            }
        }]
    });
}

