$( document ).ready(function() {
	
	getInfo();
	//readDataFile();

//  	$('#file-input-dengue-map').click(function(){
 // 		continue_spatial();

 // 	});

	
	
//	$('#pp').click(function(){//spatial
//		$('#dengue-graph').css('display','none');
//		$('#dengue-map').css('display','block');
//		$('#range-sd').css('display','block');
//		$('#range-td').css('display','none');
//		$('#dengue-option').css('display','none');
//	});
	


	
//	$('#graph-dengue-tempo').click(function(){
//		$('#myModal').modal('show');
//		dengue_tempo_mod();
//	});
	
	
	
});

function populateDropdown(){
	var cl = COLS.length-1;
	var options = '<div class="item" value="'+cl+'" data-value="'+cl+'">Provincial</div>';
	
	for(a=0; a<cl; a++){
		options += '<div class="item" value="'+a+'" data-value="'+a+'">'+COLS[a]+'</div>'
		
	}

	$('.dropdown-menu').html(options);
}

var COLS = null;
var time_dengue;
function getInfo(){
	$.ajax({
		url: "get_info",
		type: "GET"
		})
	  .done(function( data ) {
		 COLS = data['cols'];
		 time_dengue = data['dates'];
		 populateDropdown();
		 createData();
		 
		
	  })
	  .fail(function( xhr, status, errorThrown ) {
		alert( "Sorry, there was a problem!" );
	  
	});
	
}

function readDataFile(){
	// Create XHR and FileReader objects
    var xhr = new XMLHttpRequest(),
        fileReader = new FileReader();

    xhr.open("GET", MY_URL, true);
    // Set the responseType to blob
    xhr.responseType = "blob";

    xhr.addEventListener("load", function () {
        if (xhr.status === 200) {
            // onload needed since Google Chrome doesn't support addEventListener for FileReader
            fileReader.onload = function (evt) {
                // Read out file contents as a Data URL
                var contents = evt.target.result;
				parseDengue(contents);
				click_reset(COLS.length-1);

            };
            // Load blob as Data URL
            fileReader.readAsText(xhr.response);
        }
    }, false);
    // Send XHR
    xhr.send();
    window.setTimeout(putSVGs, 3000);
}

function putSVGs(){
	
	for(a=0; a< COLS.length; a++){
		sendSVG(a);
	}
}


//function readSingleFile(e) {
//  var file = e.target.files[0];
//  if (!file) {
//    return;
//  }
//  var reader = new FileReader();
//  reader.onload = function(e) {
//    var contents = e.target.result;
//    	parseDengue(contents);
//    	hide_options();

//  };
//  reader.readAsText(file);
//}

data_len = 0;
n_actual = 0;
n_forecast = 0;
arr_c = [];

function parseDengue(contents){
	if(contents.indexOf('\r') > -1){
		arr_c = contents.split('\r');	
	}else{
		arr_c = contents.split('\n');
	}
	
	// count number of rows
	data_len = arr_c.length;
	if(arr_c[data_len-1].length == 0){
		data_len = arr_c.length - 1;
	}

	// access each row and find if it has reached the forecast part
	n_actual = 0;
	var x = 0;
	while(arr_c[x].indexOf('forecast') == -1){
		n_actual++;
		x++;
	}

	n_forecast = 0;

	n_forecast = data_len - n_actual - 1;
	create_data_ave();


}

function create_data_ave(){
	var k = 1;
	for(a=0; a<COLS.length; a++){
		for(b=1; b<n_actual; b++){
			var row = arr_c[b].split(',');
			var date1 = formatDate(row[0]);
			var act1 = parseInt(row[a+1]);

			var temp = [date1, act1];
			var temp2 = [date1, null];
			fcd[COLS[a]]['actual'].push(temp);
			fcd[COLS[a]]['averages'].push(temp2);

			dengue[COLS[a]].push(act1);
		}
		
		for(c=n_actual+1; c<data_len-1; c++){
			var row = arr_c[c].split(',');
			var date1 = formatDate(row[0]);
			var frcst = parseFloat(row[k]);
			var lo1 = parseFloat(row[k+1]);
			var hi1 = parseFloat(row[k+2]);
		
			var temp = [date1, null];
			fcd[COLS[a]]['actual'].push(temp);

			var temp2 = [date1, frcst];
			fcd[COLS[a]]['averages'].push(temp2);

			var temp3 = [c-2, lo1, hi1];
			fcd[COLS[a]]['ranges'].push(temp3);
		}
		k += 3
	}

}

//Hide options

function hide_options(){
	$('.left').css('visibility','visible');
	$('#graph-dengue-tempo').css('visibility','visible');
	$('.instr-dengue').css('visibility','hidden');
	click_reset(7);
}




//function upload_data_mod(){
//	$('.modal-header').html('Please select the file containing the data.');
//}


//var dengue_type = 0;


function click_reset(dist){
	graph_data(dist);
}

var dengue = {};
var fcd = {};

function createData(){
	dengue = {};
	for(a=0; a<COLS.length; a++){
		dengue[COLS[a]] = [];
	}
	
	fcd = {};

	for(x=0; x< COLS.length; x++){
		fcd[COLS[x]] = {};
		fcd[COLS[x]]["ranges"] = [];
		fcd[COLS[x]]["averages"] = [];
		fcd[COLS[x]]["actual"] = [];
	}
	 readDataFile();
}


function graph_data(d){

	series_ff = [];
	//48
//	if(d == COLS.length){
//		for(a=0; a < COLS.length; a++)
//			series_ff.push({name: COLS[a-1], data: [dengue[COLS[a-1]][0]]});
		
//	}else{
//		series_ff.push({name: COLS[d-1], data: [dengue[COLS[d-1]][0]]});
		
//	}

	series_ff.push({name: COLS[d], data: [dengue[COLS[d]][0]]});
	createGraph('dengue-graph', d);
	
}
var svgCount = 0;
function sendSVG(a){
	
	$.ajax({
	 
		url: "graph_svgs",
		type: "POST",
		data: {'svg': createGraph('graph-svg', a), 
				'n': a+1,
				'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()
			}
		})
	  .done(function( json ) {
			svgCount++;
			completed();
	  })
	  .fail(function( xhr, status, errorThrown ) {
		alert( "Sorry, there was a problem!" );
	  
	});
}

function completed(){
	if(svgCount==COLS.length)
		$('.step-four-next').prop('disabled', false);
}

var cdd = 1;
var chart;
var series_ff = [];
function createGraph(container, d){
	
	cdd = d;
	
	var col_name = COLS[d];
	var chosen = (d == COLS.length-1) ? 'Provincial' : (col_name);

	chart = Highcharts.chart(container, {
        
        title: {
            text: data_name + ' Data'
        },
		subtitle: {
    		text: chosen
		},
		chart: {
            zoomType: 'x',
        },
		xAxis: {
			categories: time_dengue
		},
		yAxis: {
			title: {
				text: "Number of cases"
			}
		},
        tooltip: {
        crosshairs: true,
        shared: true,
		headerFormat: '<small>{point.key}</small><br>',
		},

		legend: {
		},

		series: [{
			name: 'Range',
			data: fcd[col_name]['ranges'],
			type: 'arearange',
			lineWidth: 0,
			linkedTo: ':previous',
			color: Highcharts.getOptions().colors[0],
			fillOpacity: 0.3,
			zIndex: 0,
			marker: {
				enabled: false
			}
		},{
			name: 'Model',
			data: fcd[col_name]['averages'],
			zIndex: 1,
			marker: {
				fillColor: 'white',
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[0]
			}
		}, {
			name: 'Actual',
			data: fcd[col_name]['actual'],
			zIndex: 1,
			color: 'gray',
			marker: {
				fillColor: 'gray',
				lineWidth: 2,
				lineColor: 'gray'
			}
		}]
    });        
	return chart.getSVG();
}



const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

function formatDate(yyyy_mm){
	d = new Date(yyyy_mm);
	year = d.getFullYear() + '';
	d1 = monthNames[d.getMonth()] + '-' + year.substring(2);

	return d1;


}




