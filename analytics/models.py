from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

		

		
class CustomUser(AbstractUser):
    designation = models.CharField(max_length=30)
    PROVINCES = (
        ('Pangasinan', 'Pangasinan'),
        ('Lanao del Sur', 'Lanao del Sur'),
        ('Palawan', 'Palawan'),
    )	
    province = models.CharField(max_length=20,
                                    choices=PROVINCES,
                                    default='Pangasinan')
    
    def __str__(self):
        return self.email
									
class ReportRecord(models.Model):
    user = models.ForeignKey(
      settings.AUTH_USER_MODEL,
      on_delete=models.CASCADE
    )
    stamp = models.CharField(max_length=50)
	
    def __str__(self):
        return '%s %s %s' % (self.user.province, self.user.username, self.stamp)

