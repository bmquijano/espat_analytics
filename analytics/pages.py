from django.contrib.auth.decorators import login_required
from analytics.models import ReportRecord
from django.http import JsonResponse
from django.shortcuts import render

@login_required
def history(request):
    id = int(request.user.id)
    user = request.user.username
    data = {'history': 1, 'user': user}
    rr = ReportRecord.objects.all().filter(user__pk=id)
    data['rr'] = rr

    return render(request, 'analytics/history.html', data)
	
@login_required
def about(request):
    id = int(request.user.id)
    user = request.user.username
    data = {'about': 1, 'user': user}

    return render(request, 'analytics/about.html', data)
	

@login_required
def get_history(request):
    id = int(request.POST.get('id'))
    rr = ReportRecord.objects.all().filter(user__pk=id)
    data = []
	
    for r in rr:
        data.append(r.stamp)
	
    return JsonResponse({'stamps':data})
	