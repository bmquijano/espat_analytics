from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from .report import generate_report
from django.http import JsonResponse
from analytics.models import ReportRecord
from django.contrib.auth.decorators import login_required
from rpy2.robjects import r
import time, json, os, csv, pytz, glob, zipfile
import datetime as dt
from django.contrib.auth import get_user_model, logout
from django.forms.models import model_to_dict
from .utilities import (formatted_time, get_timestamp, delete_files,
write_csv, status_check, graph_svgs, next_months, division_name, 
province, report_history, fn_template, filters)
from django.conf import settings

STATIC = settings.STATIC_ABS_PATH
MEDIA = settings.MEDIA_ABS_PATH

@login_required
def logout_view(request):	
    delete_files(request)	
    logout(request)	
    return redirect('index')

@login_required
def index(request):
    return step_one(request)

@login_required
def step_one(request):
    delete_files(request)
    if request.method == 'POST':
        myfile = request.FILES['file']
        fs = FileSystemStorage()
        request.session['data_name'] = request.POST.get("name-data", "")		
        request.session['division'] = int(request.POST.get("division", ""))
        request.session['time'] = int(request.POST.get("time", "0"))
        request.session['filter_messages'] = []		
        filename = MEDIA + request.user.username + '_data.csv'
		
        fs.save(filename, myfile)
        time.sleep(3)		
        
        if filters(request, filename):
            request.session['step'] = 1	
            return HttpResponseRedirect('/analytics/step_two')            
        else:
            data = {'var1':1, 'alpha':1, 'error':1}
            data = status_check(data, 1)
            return render(request, 'analytics/step_one.html', data)
    else:
        data = {'var1':1, 'alpha':1}
        data = status_check(data, 1)
        return render(request, 'analytics/step_one.html', data)
		
@login_required
def step_one_download(request):	
    template = int(request.GET.get('template'))
    file_path = fn_template(request,template)	
	
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
            return response
    raise Http404
	

@login_required
def step_two(request):

    if 'step' in request.session and request.session['step'] >= 1:
        request.session['step'] = 2
        data = {'var2':1, 'alpha':1}
        data = status_check(data, 2)
        return render(request, 'analytics/step_two.html', data)
    else:
        return redirect('/analytics/step_one')
	
@login_required
def step_two_ajax(request):
    username = request.user.username
    load_library();
    fn = MEDIA + request.user.username + '_data.csv'
    r("dengue_data = read.csv('"+fn+"', header=TRUE, stringsAsFactors=FALSE)")
    r("dengue_data$Date = as.Date(as.yearmon(dengue_data$date))")
    r('ggplot(dengue_data, aes(Date, total)) + geom_line() + scale_x_date("time")  + ylab("Cases ('+request.session['data_name']+')") + xlab("")')
    time.sleep(2)
    img1 = username + '_image1.png'
    r('ggsave("'+STATIC+'img/'+img1+'")')	
    normalize(request,username)
    seasonal(username)
    time.sleep(1)
    img1 = 'img/' + request.user.username + '_image1.png'
    img2 = 'img/' + request.user.username + '_image2.png'
    img3 = 'img/' + request.user.username + '_image3.png'
	
    data = {'image1':img1, 'image2':img2, 'image3':img3}
    
    return JsonResponse(data)
	
def load_library():
    r("setwd('/var/www/html/espat_analytics')")
    r("library('ggplot2')")
    r("library('forecast')")
    r("library('tseries')")
    r("library('zoo')")
    return


def seasonal(username):
    r("total_yr = ts(na.omit(dengue_data$clean_total), frequency=12)")
    r("decomp = stl(total_yr, s.window='periodic')")
    time.sleep(2)
    img3 = username + '_image3.png'
    r('png(filename="'+STATIC+'img/'+img3+'")')
    r('plot(decomp)')
    r('dev.off()') 
    return 
	
@login_required
def normalize(request,username):
    r("total_ts = ts(dengue_data[, c('total')])")
    r("dengue_data$clean_total = tsclean(total_ts)")
    r("ggplot(dengue_data, aes(Date, clean_total)) + geom_line() + ylab('Cases ("+request.session['data_name']+")') + xlab('')")
    time.sleep(2)
    img2 = username + '_image2.png'
    r('ggsave("'+STATIC+'img/'+img2+'")')	
    return
	
@login_required
def step_three_ajax(request):
    t = '12'
    if request.session['time'] == 1:
        t = '4'
        
    load_library();
    fn = request.user.username + '_data.csv'
    r("dengue_data = read.csv('"+MEDIA+fn+"', header=TRUE, stringsAsFactors=FALSE)")	
    time.sleep(1)
    r("dengue_data$Date = as.Date(as.yearmon(dengue_data$date))")
    r("total_ts = ts(dengue_data[, c('total')])")
    r("dengue_data$clean_total = tsclean(total_ts)")
    r("total_yr = ts(na.omit(dengue_data$clean_total), frequency="+t+")")
    output1 = r("auto.arima(total_ts)")
    output2 = str(output1)
    output3 = output2.replace('\n','<br/>')
    a = output2.index('(')
    b = output2.index(')')
    c = output2[a+1:b]
    d = c.split(',')
	
    data = {
        'output': output3,
        'd': d
    }
    return JsonResponse(data)

@login_required
def step_three_arima_ajax(request): #add
    p = request.GET.get('p')
    d = request.GET.get('d')
    q = request.GET.get('q')
    c = request.GET.get('c')
    
    request.session['params'] = [p,d,q,c]
	
    username = request.user.username
    load_library();
    fn = username + '_data.csv'
    r("dengue_data = read.csv('"+MEDIA+fn+"', header=TRUE, stringsAsFactors=FALSE)")	
    time.sleep(1)
    r("dengue_data$Date = as.Date(as.yearmon(dengue_data$date))")
    r("total_ts = ts(dengue_data[, c('total')])")
    r("dengue_data$clean_total = tsclean(total_ts)")
    r("total_yr = ts(na.omit(dengue_data$clean_total), frequency=12)")
    arima = "fitARIMA <- arima(total_yr, order=c("+str(p)+","+str(d)+","+str(q)+"),method='ML')"
    
    r(arima)
    fut = str(r("futureVal <- forecast(fitARIMA,h=6, level=c("+c+"))"))
  
    fn2 = username + '_forecast.png'
    fn3 = STATIC + 'img/' + fn2	
    r('autoplot(futureVal)')
    r('ggsave("'+fn3+'")')
    arima_for_each(request)

    fut2 = format_arima_output(request, fut)	
    data = {
        'output': 12,
        'p' : p,
        'd' : d,
        'q' : q,
        'fut':fut2,
        'forecast':fn2
    }
    time.sleep(3)
    #must perform arima to pass step 3
    request.session['step'] = 3
    return JsonResponse(data)

@login_required
def route_last_step(request):
    if 'step' not in request.session:
        return redirect('/analytics/step_one')
    else:
        step = request.session['step']

        if step == 1:
            return redirect('/analytics/step_one')
        elif step == 2:
            return redirect('/analytics/step_two')
        elif step == 3:
            return redirect('/analytics/step_three')
        elif step == 4:
            return redirect('/analytics/step_four')
        elif step == 5:
            return redirect('/analytics/step_five')
        else:
            return redirect('/analytics/step_six')

@login_required
def format_arima_output(request, fut): #add
    p_dates = request.session['pdates']
    fut1 = fut.split('\n') #convert to a list
    fut1.pop(0) #no header
    fut1.remove('')
    
    x = 0
    data = []
    for a in fut1:
        i = a.find('   ')
        if a == -1:
            i = 0
        temp = a[i:].split(' ')	
        temp_list = []
        temp_list.append(a[0:i].strip())	
	
        for b in temp:
            if b != '':			
                temp_list.append(b.strip())
        
        temp_list[0] = p_dates[x]		
        data.append(temp_list)
        x+=1
		
    return data

@login_required
def arima_for_each(request):
    p, d, q, c = request.session['params']
    username = request.user.username
	
    t = '12'
    if request.session['time'] == 1:
        t = '4'
	
    with open(MEDIA+username+'_data.csv', newline='') as f:
        reader = csv.reader(f)
        row1 = next(reader)
        columns = row1[1:len(row1)]
		
    time = []
    with open(MEDIA+username+'_data.csv', 'r') as fh:
        csv_data = list(csv.reader(fh))

        for row in csv_data[1:]:
            try:		
                f_date = dt.datetime.strptime(row[0],'%Y-%m').strftime('%b-%Y')		
            except ValueError:
                f_date = 'Unkown date'
            time.append(f_date)
            			
    p_months = next_months(time[-1], 6)

    request.session['forecast'] = {}
    request.session['columns'] = columns
    request.session['dates'] = time + p_months
    request.session['pdates'] = p_months

    for_cols = ['forecast']
    for col in columns:
        for_cols += [col, 'lo'+str(c), 'hi'+str(c)]	#form headers for appended data
        r(col+"_ts = ts(dengue_data[, c('"+col+"')])")
        r("dengue_data$clean_"+col+" = tsclean("+col+"_ts)")
        r(col+"_yr = ts(na.omit(dengue_data$clean_"+col+"), frequency="+t+")")
        arima = col + "fitARIMA <- arima("+col+"_yr, order=c("+str(p)+","+str(d)+","+str(q)+"),method='ML')"
    
        r(arima)
        fut = str(r(col+"futureVal <- forecast("+col+"fitARIMA,h=6, level=c("+c+"))"))
	
        request.session['forecast'][col] = format_arima_output(request, fut)
        
    request.session['for_cols'] = for_cols
 
    write_csv(request)
	
	
@login_required	
def step_three(request):
    if 'step' in request.session and request.session['step'] >= 2:
        request.session['step'] = 3
        data = {'var3':1, 'alpha':1}
        data = status_check(data, 3)
        return render(request, 'analytics/step_three.html', data)
    else:
        return redirect('/analytics/step_two')

@login_required
def step_four(request):
    if 'step' in request.session and request.session['step'] >= 3:
        request.session['step'] = 4
        data = {'var4':1}
        data = status_check(data, 4)
        data['division'] = division_name(request.session['division'])
	
        return render(request, 'analytics/step_four.html', data)
    else:
        return redirect('/analytics/step_three')


@login_required
def get_info(request):
    data = {'cols' : request.session['columns'],
            'dates': request.session['dates']}
    return JsonResponse(data)
	
@login_required
def step_five(request):
    if 'step' in request.session and request.session['step'] >= 3:
        data = {'var5':1}
        data = status_check(data, 5)
        request.session['step'] = 5
        return render(request, 'analytics/step_five.html', data)
    else:
        return redirect('/analytics/step_four')
	
@login_required
def step_six(request):
    if not 'step' in request.session:
        return redirect('/analytics/step_one')
    else:
        request.session['step'] = 6
        data = {'var6':1, 'alpha':1}
        data = status_check(data, 6)
        username = request.user.username

        return render(request, 'analytics/step_six.html', data)

@login_required
def step_six_download(request):
    username = request.user.username
    filename_time = formatted_time(False)	
    generate_report(request, username, filename_time)
    province = request.user.province.replace(' ','_')
    file_path = MEDIA + 'eSPAT1_'+username+'_report_'+province+'_'+filename_time+'.zip'	
	
    z = zipfile.ZipFile(file_path, 'w', zipfile.ZIP_DEFLATED)
    csv_raw = MEDIA + username + "_data.csv"
    csv_forecast = MEDIA + username + "_forecast_min.csv"
    pdf = MEDIA + username + "_report_"+ province + "_" + filename_time +".pdf"

    z.write(csv_raw, arcname=(province+'_'+username + "_data_"+filename_time+".csv"))
    z.write(csv_forecast, arcname=(province+'_'+username + "_forecast_min_"+filename_time+".csv"))
    z.write(pdf, arcname=(province+'_'+username + "_report_"+filename_time+".pdf"))
    z.close()
    
	
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/zip")
            response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
			
            report_history(request) #record to db
            return response
    raise Http404("No file")	
	

@login_required
def forecast_csv(request):
    f_fname = request.user.username + '_forecast_data.csv'
    file_path = MEDIA + f_fname
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="text/csv")
            return response
    raise Http404('No file found')


@login_required
def data_csv(request):
    fname = request.user.username + '_data.csv'
    file_path = MEDIA + fname	
	
    if os.path.exists(file_path):
        with open(file_path, 'r') as fh:
            reader = csv.reader(fh)
            row1 = next(reader)
            columns = row1[1:len(row1)-1]
        
		
        request.session['columns'] = columns		
        with open(file_path, 'r') as fh:
            csv_data = list(csv.reader(fh))
			
            data = {}
            data['columns'] = columns
            data['time'] = []
            data['info'] = province(request)			

            for c in columns:
                data[c] = []
		
            for row in csv_data[1:]:
                data['time'].append(row[0])
                for i, c in enumerate(columns):
                    if row[i+1] != '':				
                        data[c].append(int(row[i+1]))
            
            return JsonResponse(data)
    raise Http404('No file found')
	
