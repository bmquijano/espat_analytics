from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError

@login_required
def isAuthorized(request):
    User = get_user_model()
    u = User.objects.get(id=request.user.id)

    if 'admin' in u.designation.lower():
        return True
    else:
        return False	
	
@login_required
def provincial_admin(request):
    id = int(request.user.id)
    user = request.user.username
    User = get_user_model()
    u = User.objects.get(username=user)
 
    if isAuthorized(request):
        users = User.objects.all().filter(province=u.province).exclude(designation__iexact='provincial admin').exclude(designation__iexact='admin')
        data = {'history' : 1, 'users' : users, 'province' :u.province}
        return render(request, 'analytics/provincial_admin.html', data)
    else:
        return redirect('index')
	
@login_required
def suspend_account(request):
    id = int(request.POST.get('id'))
    User = get_user_model()
    u = User.objects.get(pk=id)
	
    if isAuthorized(request):	
        u.is_active = not u.is_active
        u.save()

    return JsonResponse({'status':u.is_active})	
	
@login_required
def delete_account(request):
    id = int(request.POST.get('id'))
    User = get_user_model()
    if User.objects.all().filter(pk=id).exists() and isAuthorized(request):
        u = User.objects.get(pk=id)
        u.delete()

    return JsonResponse({'delete':True})

@login_required
def create_account(request):
    username = request.POST.get('username')
    email = request.POST.get('email')
    first = request.POST.get('first-name')
    last = request.POST.get('last-name')
    designation = request.POST.get('designation')
    province = request.POST.get('province')
    password = request.POST.get('password')

	
    if isAuthorized(request):	
        try:
            User = get_user_model()
            u = User.objects.create(email=email, username=username, first_name=first, last_name=last, designation=designation, province=province)
            u.set_password(password)
            u.save()
        except IntegrityError as e:
            pass			
        
		
    return redirect('provincial_admin')
	
@login_required
def change_password(request):
    current_user = request.user
    password = str(request.POST.get('new_password'))
	
    if isAuthorized(request):	    	
        User = get_user_model()
        u = User.objects.get(pk=current_user.id)
        u.set_password(password)
        u.save()	
		
    return JsonResponse({'change':True})