import pytz, datetime, os, glob, csv
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from analytics.models import ReportRecord
from django.contrib.auth import get_user_model
from shutil import copyfile
from cairosvg import svg2png
from django.conf import settings

STATIC = settings.STATIC_ABS_PATH
MEDIA = settings.MEDIA_ABS_PATH

def formatted_time(flag=True):
    tz = pytz.timezone('Asia/Manila')
    ct = datetime.datetime.now(tz=tz)
	
    if flag:	
        return ct.strftime("%a, %b %d, %Y %I:%M %p")
    else:
        return ct.strftime("%a_%b_%d_%Y_%Ihr_%Mmin_%p")
		
def get_timestamp():
    timezone = pytz.timezone("Asia/Manila")
    now = datetime.datetime.now(timezone)
    formatted_time = now.strftime("%a %b-%d-%Y at %I:%M %p")
	
    return formatted_time
	

def delete_files(request):
    username = request.user.username
	
    for fn1 in glob.glob(MEDIA+username+'_*'):	
        os.remove(fn1)
		
    for fn1 in glob.glob(MEDIA+'eSPAT1_'+username+'_*'):	
        os.remove(fn1)

    for fn2 in glob.glob(STATIC+'img/'+username+'_*'):	
        os.remove(fn2)
		
		
@login_required	
def write_csv(request):
    username = request.user.username
    f_fname = username + '_forecast_data.csv'
    d_fname = username + '_data.csv'
    fm_fname = username + '_forecast_min.csv'
	
    copyfile(MEDIA+d_fname, MEDIA+f_fname)	
   
    option_csv(request, f_fname, 'a')
    option_csv(request, fm_fname, 'w')	

@login_required		
def option_csv(request, fname, mode):
    columns = request.session['columns']	
    forecast = request.session['forecast']
	
    with open(MEDIA+fname, mode, newline='') as f:	
        wr = csv.writer(f, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        wr.writerow(request.session['for_cols'])
        for a in range(6):
            temp = [' ']		    
            for col in columns: 			
                temp += forecast[col][a][1:]
        
            wr.writerow(temp)
			
def status_check(comp, num):
    for x  in range(7):
        k = 'stat' + str(x)
        if x < num:
            comp[k] = 'completed'
        else:
            comp[k] = ''
    return comp
	
@login_required
def graph_svgs(request):
    svg_code = str(request.POST.get('svg'))
    n = request.POST.get('n')
	
    fname = request.user.username + '_graph'+n+'.png'
    	
    svg2png(bytestring=svg_code,write_to=STATIC+'img/'+fname)
    return JsonResponse({'success':1})
	
def next_months(date, n):
    months = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12}
    year = int(date.split('-')[1])
    m = months[date.split('-')[0]]
    gen_months = []
    for i in range(m,m+n):
        x = i%12
        if x == 0:
            year += 1
        gen_months.append(datetime.date(year, x+1, 1).strftime('%b-%Y'))
    return gen_months
	
def division_name(code):
    if code == 0:
        return 'ILHZ'
    elif code == 1:
        return 'Legislative District'
    else:
        return 'City/Municipality' 	
		
def province(request):
    div = request.session['division']
    User = get_user_model()
    user = request.user.username
    u = User.objects.get(username=user)
    p = u.province
    
    shp = p.lower().replace(' ','') + '_' + str(div)
	
    d = ''

    if div == 0:
        d = 'Healthzone'
    elif div == 1:
        d = 'District'
    else:
        d = 'Municipality'
	
    return {'shp':shp, 'province':p, 'div': d};
	
		
def fn_template(request,template):
    province = request.user.province.replace(' ','_')
    filepath = ''
	
    if template == 0:
        file_path = MEDIA + province + "_healthzones.csv"		
    elif template == 1:
        file_path = MEDIA + province + "_districts.csv"	
    else:
        file_path = MEDIA + province + "_municipalities.csv"	
		
    return file_path	
	
def filters(request, up_file):
    f1 = filter_headers(request, up_file)
    f2 = filter_dates(request, up_file)
    f3 = filter_data(request, up_file)
	
    if f1 and f2 and f3:
        return True
    else:
        return False	
	
	
def filter_headers(request, up_file):
    template = int(request.session['division'])
    temp_file = fn_template(request, template)
    col1 = None
    col2 = None	
    with open(up_file, newline='') as f1:
        reader1 = csv.reader(f1)
        row1 = next(reader1)
        col1 = row1[:len(row1)]
    
    with open(temp_file, newline='') as f2:
        reader2 = csv.reader(f2)
        row2 = next(reader2)
        col2 = row2[:len(row2)]
		
    #not the same column names

    if col1 != col2:
        request.session['filter_messages'].append('Column names do not match up.')	
        return False	
    else:
   	    return True

def filter_dates(request, up_file):		
    with open(up_file, 'r') as fh:
        csv_data = list(csv.reader(fh))
        
        for row in csv_data[1:]:
            if row[0] != '':		
                try:		
                    f_date = datetime.datetime.strptime(row[0],'%Y-%m').strftime('%b-%Y')		
                except ValueError:
                    request.session['filter_messages'].append('Not all dates are in valid format')
                    return False	
    return True
	
def filter_data(request, up_file):
    num = 0
    with open(up_file, newline='') as ff:
        reader = csv.reader(ff)
        row1 = next(reader)
        num = len(row1)


    with open(up_file, 'r') as fh:
        csv_data = list(csv.reader(fh))

        for x in range(1,num):		
            for row in csv_data[1:]: #skip header
                if row[x] is None or row[x] == '' or float(row[x]) < 0.0 or not row[x].isdigit():
                    print(row[x])				
                    request.session['filter_messages'].append('Data may contain null, negative numbers or non-numeric values.')
                    return False			
    
    return True	
	
def report_history(request):
    rr = ReportRecord.objects.create(user=request.user, stamp=get_timestamp())
    rr.save()