from django.urls import path

from . import views, account, pages

urlpatterns = [
    path('', views.index, name='index'),
    path('step_one_download', views.step_one_download, name='step_one_download'),
    path('step_one', views.step_one, name='step_one'),
    path('step_two', views.step_two, name='step_two'),
    path('step_three', views.step_three, name='step_three'),
    path('step_four', views.step_four, name='step_four'),
    path('step_five', views.step_five, name='step_five'),
    path('step_six', views.step_six, name='step_six'),	
    path('step_two_ajax', views.step_two_ajax, name='step_two_ajax'),
    path('step_three_ajax', views.step_three_ajax, name='step_three_ajax'),
    path('step_three_arima_ajax', views.step_three_arima_ajax, name='step_three_arima_ajax'),
    path('step_six_download', views.step_six_download, name='step_six_download'),
    path('history', pages.history, name='history'),
    path('about', pages.about, name='about'),	
    path('forecast_csv', views.forecast_csv, name='forecast_csv'),	
    path('data_csv', views.data_csv, name='data_csv'),
    path('route_last_step', views.route_last_step, name='steps'),
    path('graph_svgs', views.graph_svgs, name='graph_svgs'),
    path('get_info', views.get_info, name='get_info'),
    path('get_history', pages.get_history, name='get_history'),
    path('logout', views.logout_view, name='logout_account'),	
    path('suspend_account', account.suspend_account, name='suspend_account'),
    path('delete_account', account.delete_account, name='delete_account'),		
    path('create_account', account.create_account, name='create_account'),	
    path('provincial_admin', account.provincial_admin, name='provincial_admin'),
    path('change_password', account.change_password, name='change_password'),	
]